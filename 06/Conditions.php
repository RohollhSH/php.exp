<?php
include "../common/common.php";
$r = rand(0,100);

myPrint("1"+"2");
myPrint("1"*"2");

$arr1 = [1,2,3];
$arr2 = [1,3,2];
if($arr1 === $arr2){
	echo "if block";
}else{
	echo "else block";
}
myPrint("--------------------------");
$x = 0;
if(++$x){
	echo "if block";
}else {
	echo "else block";
}

/*
 1 and 3    => true

 1 & 3
1=>  01
2=>  10
&    00  => 0 : false
*/

myPrint("--------------------------");
myPrint($r);
if($r < 10){
	myPrint("<10");
}else if($r < 30){
	myPrint("<30");
}else if($r < 70){
	myPrint("<70");
}else{
	myPrint(">70");
}

$age = $r;
if($age < 18){
	$a = "access denied for -18th !";
}else if($age > 70){
	$a = "access denied for +70th !";
}else{
	$a = "access allowed !";
}

$a = ($age < 18) ? "access denied for -18th !" : ($age > 70) ? "access denied for +70th !" : "access allowed !";

$sexuality = "1";
switch ($sexuality){
	case "m":
	case "1":
	case "male":
		myPrint("Male");
		break;
	case "0":
	case "f":
	case "female":
		myPrint("Female") ;
		break;
	default:
		myPrint("others...");
}

/*
if($r < 10){
	myPrint("<10");
}else if($r < 30){
	myPrint("<30");
}else if($r < 70){
	myPrint("<70");
}else{
	myPrint(">70");
}
*/
switch ($r){
	case $r < 10 :
		myPrint("<10");
		break;
	case $r < 30 :
		myPrint("<30");
		break;
	case $r < 70 :
		myPrint("<70");
		break;
	default  :
		myPrint(">70");
}