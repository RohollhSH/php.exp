<?php
define( "USER_ROLE_ADMIN", "admin" );
define( "USER_ROLE_AUTHOR", "author" );
define( "USER_ROLE_SUBSCIBER", "subscriber" );


function getUsersInfo(){
	global $conn;
	$sql = "select * from users";
	$result = mysqli_query($conn,$sql);

	/*	while($row = mysqli_fetch_assoc($result)){
			var_dump($row);
		}*/
	// return all records
	return mysqli_fetch_all($result);
}
function getUserInfo($email){
	if(!userExists($email)){
		return NULL;
	}
	global $conn;
	$sql = "select * from users where email='$email'";
	$result = mysqli_query($conn,$sql);
	// return single records
	return mysqli_fetch_assoc($result);
}


function userExists($email){
	global $conn;
	$sql = "select count(*) as c from users where email='$email'";
	$result = mysqli_query($conn,$sql);
	$count = mysqli_fetch_assoc($result);
	return $count['c'];
}


function register( $email, $password, $display_name, $role = USER_ROLE_SUBSCIBER ) {
	if(userExists($email)){
		return -1;
	}
	global $conn;
	$passwordHash = password_hash($password,PASSWORD_BCRYPT);
	$sql = "INSERT INTO `users` (`email`, `password`, `display_name`, `role`) VALUES ('$email', '$passwordHash', '$display_name', '$role');";
	$result = mysqli_query($conn,$sql);
	return mysqli_insert_id();
}


function checkPassword($pwd, &$errors) {
	$errors_init = $errors;

	if (empty($pwd)) {
		$errors[] = "رمز عبور وارد نشده است";
	}
	if (strlen($pwd) < 8) {
		$errors[] = "رمز عبور کوتاه است";
	}
	if (!preg_match("#[0-9]+#", $pwd)) {
		$errors[] = "حداقل یک عدد";
	}
	if (!preg_match("#[a-zA-Z]+#", $pwd)) {
		$errors[] = "حداقل یک حرف الفبا";
	}
	return ($errors == $errors_init);
}

function login($email,$password){
	if(!userExists($email)){
		return -1;
	}
	$userInfo = getUserInfo($email);

	if(password_verify($password,$userInfo['password'])){
		$_SESSION['login'] = $email ;
		header("Location: index.php");
		return true;
	}

	return false;

	//	var_dump($userInfo);
}

function user_logged_in(){
	if(empty($_SESSION['login'])){
		return false;
	}
	return true;
}

function logout(){
	$_SESSION['login'] = '' ;
	unset($_SESSION['login']);
}