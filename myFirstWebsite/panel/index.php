<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>پنل کاربری</title>
    <link rel="stylesheet" href="../assets/css/normalize.css">
    <link rel="stylesheet" href="../assets/css/milligram-rtl.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
    <link rel="stylesheet" href="../style.css">

</head>
<body>
<div class="continer">
    <div class="row header">
        <h1>پنل کاربری</h1>
    </div>

    <div class="row">
        <div class="column column-25 sidebar">
            <ul>
                <li><a href="/">داشبورد</a></li>
                <li><a href="/?page=write-post">ارسال مقاله</a></li>
            </ul>
        </div>
        <div class="column column-72 column-offset-3 content">
            محتوا
        </div>
        </div>
</div>

</body>
</html>